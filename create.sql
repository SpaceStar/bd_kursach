CREATE TABLE lines (
	id serial PRIMARY KEY,
	scheme_number smallint NOT NULL CHECK (scheme_number > 0) UNIQUE,
	scheme_color varchar(20) NOT NULL UNIQUE
);

CREATE TABLE depots (
	id serial PRIMARY KEY,
	line_id integer NOT NULL REFERENCES lines(id),
	name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE trains (
	id serial PRIMARY KEY,
	depot_id integer NOT NULL REFERENCES depots(id)
);

CREATE TABLE carriage_types (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE carriage_models (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE carriages (
	id serial PRIMARY KEY,
	type_id integer NOT NULL REFERENCES carriage_types(id),
	model_id integer NOT NULL REFERENCES carriage_models(id),
	train_id integer REFERENCES trains(id),
	depot_id integer REFERENCES depots(id),
	position smallint CHECK (position > 0),
	broken boolean NOT NULL DEFAULT FALSE,
	CHECK (((train_id IS NULL) AND (depot_id IS NOT NULL)) OR
		((train_id IS NOT NULL) AND (depot_id IS NULL)))
);

CREATE OR REPLACE FUNCTION check_carriage_position(train integer, position_in_train smallint)
RETURNS boolean AS $$
SELECT NOT EXISTS (
	SELECT * FROM carriages WHERE (train_id = train) AND (position = position_in_train)
) $$ LANGUAGE SQL;

ALTER TABLE carriages ADD
	CHECK ((train_id IS NULL) OR ((position IS NOT NULL) AND (check_carriage_position(train_id, position))));

CREATE TABLE station_states (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE stations (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL UNIQUE,
	open_type boolean NOT NULL DEFAULT TRUE,
	line_id integer NOT NULL REFERENCES lines(id),
	state_id integer NOT NULL REFERENCES station_states(id)
);


CREATE TABLE paths (
	id serial PRIMARY KEY,
	station_id integer NOT NULL REFERENCES stations(id),
	to_station integer NOT NULL REFERENCES stations(id),
	time time,
	UNIQUE (station_id, to_station),
	CHECK (station_id <> to_station)
);

CREATE OR REPLACE FUNCTION check_path_exists(st1 integer, st2 integer)
RETURNS boolean AS $$
SELECT NOT EXISTS (
	SELECT * FROM paths WHERE (station_id = st1) AND (to_station = st2)
) $$ LANGUAGE SQL;

ALTER TABLE paths ADD
	CHECK (check_path_exists(to_station, station_id));


CREATE TABLE people (
	id serial PRIMARY KEY,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	passport_series varchar(10) NOT NULL,
	passport_number bigint NOT NULL CHECK (passport_number > 0),
	birth_date date NOT NULL,
	UNIQUE (passport_series, passport_number),
	CHECK (birth_date < CURRENT_DATE)
);

CREATE TABLE positions (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE workers (
	people_id integer PRIMARY KEY REFERENCES people(id),
	position_id integer NOT NULL REFERENCES positions(id),
	available boolean NOT NULL DEFAULT TRUE,
	contract_start date NOT NULL,
	contract_end date,
	CHECK ((contract_end IS NULL) OR (contract_end - contract_start > 0))
);

CREATE TABLE station_worker (
	worker_id integer REFERENCES workers(people_id),
	station_id integer REFERENCES stations(id),
	PRIMARY KEY (worker_id, station_id)
);

CREATE TABLE card_types (
	id serial PRIMARY KEY,
	name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE cards (
	id serial PRIMARY KEY,
	people_id integer NOT NULL REFERENCES people(id),
	type_id integer NOT NULL REFERENCES card_types(id),
	start_date date NOT NULL,
	end_date date,
	CHECK ((end_date IS NULL) OR (end_date - start_date > 0))
);
