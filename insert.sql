INSERT INTO lines(scheme_number, scheme_color) VALUES
	(1, 'red'),
	(2, 'blue'),
	(3, 'green'),
	(4, 'orange'),
	(5, 'violet');

INSERT INTO depots(line_id, name)
	SELECT id, 'депо допетровское' FROM lines WHERE scheme_number = 1;
INSERT INTO depots(line_id, name)
	SELECT id, 'приозёрское депо' FROM lines WHERE scheme_number = 2;
INSERT INTO depots(line_id, name)
	SELECT id, 'депо имени Ленина' FROM lines WHERE scheme_number = 3;

INSERT INTO trains(depot_id)
	SELECT id FROM depots WHERE name = 'депо допетровское';
INSERT INTO trains(depot_id)
	SELECT id FROM depots WHERE name = 'приозёрское депо';

INSERT INTO carriage_types(name) VALUES
	('локомотив'),
	('обыкновенный'),
	('старый локомотив');

INSERT INTO carriage_models(name) VALUES
	('81-722.3'),
	('607-01'),
	('81-722');

INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 1
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'локомотив' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 8
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'локомотив' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 2
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 3
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 4
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 5
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 6
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 1, 7
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';

INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 1
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'локомотив' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 8
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'локомотив' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 2
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 3
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 4
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 5
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 6
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';
INSERT INTO carriages(type_id, model_id, train_id, position)
	SELECT carriage_types.id, carriage_models.id, 2, 7
	FROM carriage_types, carriage_models
	WHERE carriage_types.name = 'обыкновенный' AND
		carriage_models.name = '81-722.3';

INSERT INTO station_states(name) VALUES
	('открыта'),
	('закрыта'),
	('закрыта на вход'),
	('закрыта на выход'),
	('закрыта на ремонт'),
	('закрыта на проверку');

INSERT INTO stations(name, line_id, state_id)
	SELECT 'невский проспект', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'blue' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'сенная площадь', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'blue' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'спасская', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'orange' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'достоевская', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'orange' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'владимирская', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'red' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'площадь восстания', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'red' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'маяковская', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'green' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'гостиный двор', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'green' AND
		station_states.name = 'открыта';
INSERT INTO stations(name, line_id, state_id)
	SELECT 'садовая', lines.id, station_states.id
	FROM lines, station_states WHERE
		lines.scheme_color = 'violet' AND
		station_states.name = 'открыта';

INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:01:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'невский проспект' AND
		st2.name = 'гостиный двор';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:01:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'площадь восстания' AND
		st2.name = 'маяковская';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:01:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'достоевская' AND
		st2.name = 'владимирская';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:01:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'спасская' AND
		st2.name = 'садовая';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:01:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'спасская' AND
		st2.name = 'сенная площадь';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:01:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'садовая' AND
		st2.name = 'сенная площадь';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:04:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'гостиный двор' AND
		st2.name = 'маяковская';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:04:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'площадь восстания' AND
		st2.name = 'владимирская';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:04:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'достоевская' AND
		st2.name = 'спасская';
INSERT INTO paths(station_id, to_station, time)
	SELECT st1.id, st2.id, TIME '00:04:00'
	FROM stations AS st1, stations AS st2
	WHERE st1.name = 'невский проспект' AND
		st2.name = 'сенная площадь';

INSERT INTO people(first_name, last_name, passport_series, passport_number, birth_date)
	VALUES ('Иван', 'Петров', '4015', 523186, DATE '1986-06-12'),
	('Петр', 'Иванов', '4015', 465213, DATE '1998-03-21'),
	('Жакшылык', 'Нурланов', 'AC', 2468324, DATE '1997-10-03');

INSERT INTO positions(name) VALUES
	('машинист'),
	('кассир');

INSERT INTO workers(people_id, position_id, contract_start, contract_end)
	SELECT people.id, positions.id, DATE '2017-10-26', DATE '2020-10-26'
	FROM people, positions WHERE
		people.passport_series = '4015' AND
		people.passport_number = 523186 AND
		positions.name = 'машинист';
INSERT INTO workers(people_id, position_id, contract_start, contract_end)
	SELECT people.id, positions.id, DATE '2017-11-02', DATE '2022-11-02'
	FROM people, positions WHERE
		people.passport_series = 'AC' AND
		people.passport_number = 2468324 AND
		positions.name = 'кассир';

INSERT INTO station_worker(worker_id, station_id)
	SELECT workers.people_id, stations.id
	FROM workers INNER JOIN people ON (workers.people_id = people.id), stations WHERE
		people.passport_series = 'AC' AND
		people.passport_number = 2468324 AND
		stations.name = 'достоевская';

INSERT INTO card_types(name) VALUES
	('карта сотрудника'),
	('подорожник'),
	('студенческий проездной билет'),
	('ученический проездной билет');

INSERT INTO cards(people_id, type_id, start_date, end_date)
	SELECT people.id, card_types.id, DATE '2018-01-01', DATE '2018-12-31'
	FROM people CROSS JOIN card_types WHERE
		((people.passport_series = '4015' AND
		people.passport_number = 523186) OR
		(people.passport_series = 'AC' AND
		people.passport_number = 2468324)) AND
		card_types.name = 'карта сотрудника';
INSERT INTO cards(people_id, type_id, start_date, end_date)
	SELECT people.id, card_types.id, DATE '2010-06-01', NULL
	FROM people CROSS JOIN card_types WHERE
		((people.passport_series = '4015' AND
		people.passport_number = 523186) OR
		(people.passport_series = 'AC' AND
		people.passport_number = 2468324) OR
		(people.passport_series = '4015' AND
		people.passport_number = 465213)) AND
		card_types.name = 'подорожник';
INSERT INTO cards(people_id, type_id, start_date, end_date)
	SELECT people.id, card_types.id, DATE '2017-12-25', DATE '2018-01-25'
	FROM people CROSS JOIN card_types WHERE
		people.passport_series = '4015' AND
		people.passport_number = 465213 AND
		card_types.name = 'студенческий';
